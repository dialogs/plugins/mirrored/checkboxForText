# checkboxForText
Allow you to add a do not know checkbox or do not want checkbox to text input (text and numeric)

## Documentation
Before activate this plugin you need to get and activate toolsSmartDomDocument plugin.

This plugin and his documentation is WIP

## Copyright
- Copyright © 2016 Denis Chenu <http://sondages.pro>
- Licence : GNU General Public License <https://www.gnu.org/licenses/gpl-3.0.html>
